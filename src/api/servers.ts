import { ApiRequest } from 'hooks/useApi';

export interface IServer {
  name: string;
  distance: number;
}

export const getServers = (): ApiRequest<IServer[]> => async ({ apiClient, signal }) => {
  const url = 'servers';
  return await apiClient.get(url, { signal }).json<IServer[]>();
};
