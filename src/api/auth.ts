import { ApiRequest } from 'hooks/useApi';

export interface IUser {
  token: string | null;
}

export const authUser = (username: string, password: string): ApiRequest<IUser> => async ({ apiClient, signal }) => {
  const url = 'tokens';

  return await apiClient.post(url, { json: { username, password }, signal }).json<IUser>();
};
