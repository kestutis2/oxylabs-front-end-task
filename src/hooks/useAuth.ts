import { useContext, useState } from 'react';
import { useHistory } from 'react-router';

import { useApi, useAbortController } from './useApi';
import { authUser, IUser } from '../api/auth';
import { UserContext, actionTypes } from '../context/UserContextProvider';

interface State {
  data: IUser | null;
  errors: any;
  loading: boolean;
}

const initialState: State = {
  data: null,
  errors: null,
  loading: false
};

const loadStart = () => (prevState: State): State => ({
  ...prevState,
  loading: true
});

const loadSuccess = (data: IUser) => (prevState: State): State => ({
  ...prevState,
  data,
  loading: false
});

const loadFailure = (errors: any) => (prevState: State): State => ({
  ...prevState,
  errors,
  loading: false
});

export const useAuth = () => {
  const api = useApi();
  const history = useHistory();
  const { dispatch } = useContext(UserContext);
  const { signal } = useAbortController();

  const [state, setState] = useState(initialState);
  const login = async (username: string, password: string) => {
    try {
      setState(loadStart());
      const data = await api.call(authUser(username, password));
      dispatch({
        type: actionTypes.singIn,
        payload: data.token
      });
      localStorage.setItem('token', data.token);
      history.replace('/');
      if (!signal.aborted) {
        setState(loadSuccess(data));
      }
    } catch (ex) {
      if (!signal.aborted) {
        console.error('Failed to load data', ex);
        setState(loadFailure(ex));
      }
    }
  };

  const logout = () => {
    localStorage.removeItem('token');
    dispatch({
      type: actionTypes.signOut
    });
  };

  return {
    ...state,
    login,
    logout
  };
};
