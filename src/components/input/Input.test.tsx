import React from 'react';
import { mount } from 'enzyme';
import Input from './Input';

describe('<Input />', () => {
  it('should render input with correct props', () => {
    const wrapper = mount(<Input loading required placeholder="placeholder" />);
    expect(wrapper.props().required).toEqual(true);
    expect(wrapper.props().loading).toEqual(true);
    expect(wrapper.props().placeholder).toEqual('placeholder');
  });
});
