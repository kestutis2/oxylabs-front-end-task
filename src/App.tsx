import React from 'react';
import { Route, BrowserRouter, Switch } from 'react-router-dom';

import HomePage from './views/HomePage/HomePage';
import PrivateRoute from './views/ProtectedRoute';
import Login from './views/Login/Login';
import { UserContextProvider } from './context/UserContextProvider';
import { path } from './paths';

const App: React.FC = () => {
  return (
    <BrowserRouter>
      <UserContextProvider>
        <Switch>
          <Route path={path.login}>
            <Login />
          </Route>
          <PrivateRoute component={HomePage} path={path.homepage} />
        </Switch>
      </UserContextProvider>
    </BrowserRouter>
  );
};

export default App;
