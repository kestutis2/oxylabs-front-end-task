import React, { FormEvent, useContext } from 'react';
import { useHistory } from 'react-router';
import clsx from 'clsx';

import Input from '../../components/input/Input';
import Button from '../../components/button/Button';
import { UserContext } from '../../context/UserContextProvider';
import { useAuth } from '../../hooks/useAuth';
import { path } from '../../paths';
import logo from '../../styles/assets/logo-testio-white.svg';

import styles from './Login.module.scss';

const formFields = {
  username: 'username',
  password: 'password'
};

const Login: React.FC = () => {
  const { login, loading, errors } = useAuth();
  const { state } = useContext(UserContext);
  const history = useHistory();
  const onFormSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const formData = new FormData(e.target as HTMLFormElement);
    const username = formData.get(formFields.username) as string;
    const password = formData.get(formFields.password) as string;
    login(username, password);
  };

  if (state.token) {
    history.push(path.homepage);
  }

  return (
    <div className={styles['container']}>
      <img src={logo} alt="Testio logo" className={styles['logo']} />
      <div className={styles['error-message']}>{!!errors && `Error has occured: ${errors}`}</div>
      <form className={styles['form']} onSubmit={onFormSubmit}>
        <Input
          type="text"
          required
          disabled={loading}
          name={formFields.username}
          placeholder="Username"
          className={clsx(styles['form__control'], styles['form__control--username'])}
        />
        <Input
          type="password"
          required
          disabled={loading}
          name={formFields.password}
          placeholder="Password"
          className={clsx(styles['form__control'], styles['form__control--password'])}
        />
        <Button className={styles['form__control']} disabled={loading} loading={loading} type="submit">
          Log in
        </Button>
      </form>
    </div>
  );
};

export default Login;
