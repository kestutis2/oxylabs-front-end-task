import React from 'react';
import clsx from 'clsx';
import Button from 'components/button/Button';
import { useServers } from '../../hooks/useServers';
import { useAuth } from '../../hooks/useAuth';

import logo from '../../styles/assets/logo-testio-dark.svg';
import icon from '../../styles/assets/icon-leave.svg';
import styles from './HomePage.module.scss';

const HomePage: React.FC = () => {
  const { data } = useServers();
  const { logout } = useAuth();
  return (
    <div className={styles['container']}>
      <div className={styles['header']}>
        <img src={logo} className={styles['header__logo']} alt="Testio logo" />
        <Button size="small" color="secondary" icon={<img src={icon} />} onClick={() => logout()}>
          Log out
        </Button>
      </div>
      <div></div>
      <div className={clsx(styles['list__row'], styles['list__row--dark'])}>
        <span>Server</span>
        <span>Distance</span>
      </div>
      {data && (
        <div className={styles['list']}>
          {data.map((server, index) => (
            <div key={index} className={styles['list__row']}>
              <span>{server.name}</span>
              <span>{server.distance} km</span>
            </div>
          ))}
        </div>
      )}
    </div>
  );
};

export default HomePage;
