import React, { createContext, useReducer, Dispatch } from 'react';
import { ActionMap } from '../utils/helper';
import { IUser } from '../api/auth';

type ActionTypes = {
  SIGN_IN: string;
  SIGN_OUT: null;
};

export const actionTypes = {
  singIn: 'SIGN_IN',
  signOut: 'SIGN_OUT'
} as const;

const initialState: IUser = {
  token: localStorage.getItem('token') || null
};

type Actions = ActionMap<ActionTypes>[keyof ActionMap<ActionTypes>];

const reducer = (state = initialState, action: Actions): IUser => {
  switch (action.type) {
    case actionTypes.singIn:
      return {
        token: action.payload
      };
    case actionTypes.signOut:
      return { token: null };
    default:
      return state;
  }
};

export const UserContext = createContext<{
  state: IUser;
  dispatch: Dispatch<Actions>;
}>({
  state: initialState,
  dispatch: () => null
});

export const UserContextProvider: React.FC = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  return <UserContext.Provider value={{ state, dispatch }}>{children}</UserContext.Provider>;
};
