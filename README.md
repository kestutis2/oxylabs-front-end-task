## Andrius Cimermonas OxyLabs FE task

___

### Features

-   React TypeScript app made from scratch using `webpack`
-   Responsive layout and general styling achieved by using `SCSS` and `BEM`
-   Multi browser compatibility achieved by using multiple polyfills
-   Global state management using only `useContext` and `useReducer` hooks
-   Custom API requests using `fetch()` and abort controller
-   Unit-tests with `Jest` and `Enzyme`

### Available scripts

-   `npm install` - Installs front-end dependencies and generates required files
-   `npm start` - Starts development servers
-   `npm run test` - Runs unit tests
-   `npm run test:watch` - Runs TDD mode, runs the tests on every file-change