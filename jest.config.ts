module.exports = {
  testMatch: ['**/?(*.)+(spec|test).[jt]s?(x)'],
  testURL: 'http://localhost/',
  setupFiles: ['<rootDir>/tests/setup.ts'],
  transform: {
    '^.+\\.tsx?$': 'ts-jest',
    '.+\\.(css|styl|less|sass|scss|png|jpg|ttf|woff|woff2|svg)$': 'jest-transform-stub'
  },
  moduleDirectories: ['node_modules', 'src'],
  moduleFileExtensions: ['js', 'json', 'jsx', 'ts', 'tsx'],
  globals: {
    'ts-jest': {
      diagnostics: {
        warnOnly: true
      }
    }
  },
  collectCoverage: true,
  coverageProvider: 'v8',
  collectCoverageFrom: ['src/**/*.{ts,tsx}', '!**/__tests__/**', '!**/__mocks__/**'],
  coverageReporters: ['json', 'lcov', 'clover'],
  preset: 'ts-jest'
};
